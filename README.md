<p align="center">
<img src="https://gitlab.com/CypherExtended/platform_manifest/raw/parfait-release/Icon-gitlab.png" width="200px" height="200px" > 
</p>

Cypher Extended Oreo
===========

How to Build?
-------------

To initialize your local repository using the AospExtended trees, use a 
command like this:

```bash
  repo init -u https://gitlab.com/CypherExtended/platform_manifest.git -b parfait-release
```
  
Then to sync up:
----------------

```bash
  repo sync -c -jx --force-sync --no-clone-bundle --no-tags
```
Finally to build:
-----------------

```bash
  . build/envsetup.sh
  lunch aoscp_device_codename-userdebug
  brunch device_codename
```



